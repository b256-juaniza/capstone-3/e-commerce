import '../components/comp.css'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

function Login() {
  return (
  	<Container>
			<div style={{marginTop: "10vh"}} className="d-flex flex-row justify-content-center">
		    <Form style={{background: "rgba(0, 0, 0, 0.2)", width: "30vw"}}
		     className="col-3 p-5 m-5 d-flex flex-column align-items-center">
		      <Form.Group style={{width: "35vh"}} className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Name: </Form.Label>
		        <Form.Control type="text" placeholder="Enter Name" />
		      </Form.Group>
		      <Form.Group style={{width: "35vh"}} className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Email address: </Form.Label>
		        <Form.Control type="email" placeholder="Enter email" />
		      </Form.Group>
		      <Form.Group style={{width: "35vh"}} className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Password: </Form.Label>
		        <Form.Control type="password" placeholder="Password" />
		      </Form.Group>
		      <Form.Group style={{width: "35vh"}} className="mb-3" controlId="formBasicPassword">
		        <Form.Label>Verify Password: </Form.Label>
		        <Form.Control type="password" placeholder="Verify Password" />
		      </Form.Group>
		      <Button variant="primary" type="submit">
		        Submit
		      </Button>
		    </Form>
	    </div>
    </Container>
  );
}

export default Login;