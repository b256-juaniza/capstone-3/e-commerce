import '../components/comp.css'
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import swal from 'sweetalert2';



export default function Dashboard() {

		const [name, setName] = useState("");
		const [description, setDescription] = useState("");
		const [price, setPrice] = useState(0);
		const [products, setProducts] = useState([]);
		const [prodName, setProdName] = useState("");
		const [prodDesc, setProdDesc] = useState("");
		const [prodPrice, setProdPrice] = useState(0);

			
			function getProd(){
				getProduct()
				if (products.length !== 0) {
					setProdName(products[0].name);
					setProdDesc(products[0].description);
					setProdPrice(products[0].price);
				}	
			}
	function create(e) {
		e.preventDefault();

		fetch('http://localhost:4000/products/create', {
			method: "POST",
			headers: {
				'Content-Type' : 'Application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			swal.fire({
				title: "Created Successfully",
				icon: "success",
				text: "The Product is now Availabe"
			})
		})
	}
		const getProduct = () => {

		    fetch('http://localhost:4000/products/active', {
		    })
		    .then(res => res.json())
		    .then(data => {
		      console.log(data);
		      setProducts(data);
		    })
		  }
	return (
		<Container>
			<h1>Create Product</h1>
			<div style={{marginTop: "10vh"}} className="d-flex flex-row justify-content-center">
		    <Form onSubmit={e => create(e)} style={{background: "rgba(0, 0, 0, 0.2)", width: "30vw"}}
		     className="col-3 p-5 m-5 d-flex flex-column align-items-center">
		      <Form.Group style={{width: "35vh"}} className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Product Name: </Form.Label>
		        <Form.Control type="text" placeholder="" value={name} onChange={e => setName(e.target.value)}/>
		      </Form.Group>
		      <Form.Group style={{width: "35vh"}} className="mb-3" controlId="formBasicEmail">
		        <Form.Label>Description: </Form.Label>
		        <Form.Control type="text" placeholder="" value={description} onChange={e => setDescription(e.target.value)}/>
		      </Form.Group>
		      <Form.Group style={{width: "35vh"}} className="mb-3" controlId="formBasicPassword">
		        <Form.Label>price: </Form.Label>
		        <Form.Control type="number" placeholder="" value={price} onChange={e => setPrice(e.target.value)}/>
		      </Form.Group>
		      <Button variant="primary" type="submit">
		        Submit
		      </Button>
		    </Form>
		    <div style={{top: "100vh"}} className="">
				<table className="" style={{width: "60vw"}}>
			    	<thead className="text-center fs-1">Product List</thead>
			    	<tbody>
			    		<th className="text-center">Name</th>
			    		<th className="text-center">Description</th>
			    		<th className="text-center">Price</th>
			    		<tr>
			    			<td className="border-1">{prodName}</td>
			    			<td className="border-1">{prodDesc}</td>
			    			<td className="border-1">{prodPrice}</td>
			    		</tr>
			    		<tr>
			    			<td className="border-1"> </td>
			    			<td className="border-1"> </td>
			    			<td className="border-1">0</td>
			    		</tr>
			    		<tr>
			    			<td className="border-1"> </td>
			    			<td className="border-1"> </td>
			    			<td className="border-1">0</td>
			    		</tr>
			    		<tr>
			    			<td className="border-1"> </td>
			    			<td className="border-1"> </td>
			    			<td className="border-1">0</td>
			    		</tr>
			    	</tbody>
			    	<Button style={{marginLeft: "20vw", marginTop: "2vh"}} onClick={e => getProd()} variant="primary" type="submit">
			        	Get Product
			      	</Button>
			    </table>
			</div>
	    </div>
    </Container>
	)
}