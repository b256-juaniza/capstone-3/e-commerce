import './App.css';
import { Container } from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {useState, useEffect} from 'react';
import AppNavBar from './components/AppNavBar';
import { UserProvider } from './userContext';
import Home from './page/Home';
import Login from './page/Login';
import Register from './page/Register';
import Dashboard from './page/Dashboard';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  })

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
    <Router>
      <AppNavBar />
      <Container>
        <Routes>
           <Route path="/Home" element={<Home />} />
           <Route path="/Login" element={<Login />} />
           <Route path="/Signup" element={<Register />} />
           <Route path="/Dashboard" element={<Dashboard />} />
        </Routes>
      </Container>
    </Router>
    </UserProvider>
  );
}

export default App;
