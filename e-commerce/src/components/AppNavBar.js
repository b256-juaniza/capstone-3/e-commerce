import './comp.css';
import {Container, Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom'; 
import { useContext } from 'react';
import UserContext from '../userContext';


export default function AppNavBar() {
		const { user } = useContext(UserContext);
	return (
		<Navbar style={{padding: "0"}} className="fw-bold fs-5 opacity" expand="lg">
      <Container>
        <Navbar.Brand className="fs-3">MyS-Jersey</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav justify variant="tabs" className="ms-auto">
          		{
          			(user.id === null) ? 
          				 <>
	          				<Nav.Link style={{paddingBottom: "2vh", marginTop: "20px", width: "15vh"}} className="link trans" as={NavLink} to="/Signup">Sign-up</Nav.Link>
			        			<Nav.Link style={{paddingBottom: "2vh", marginTop: "20px", width: "15vh"}} className="link trans" as={NavLink} to="/Login">Log-in</Nav.Link>
	          			</>
          			:
          				<> 
	          				<Nav.Link style={{paddingBottom: "2vh", marginTop: "20px", width: "15vh"}} className="link trans" as={NavLink} to="/Home">Home</Nav.Link>   
										<Nav.Link style={{paddingBottom: "2vh", marginTop: "20px", width: "15vh"}} className="link trans" as={NavLink} to="/Services">Services</Nav.Link>
			            	<Nav.Link style={{paddingBottom: "2vh", marginTop: "20px", width: "15vh"}} className="link trans" as={NavLink} to="/About">About</Nav.Link>
			            	<Nav.Link style={{paddingBottom: "2vh", marginTop: "20px", width: "15vh"}} className="link trans" as={NavLink} to="/Contact">Contact</Nav.Link>
			            </>	
          		}
		            	                  			
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
	)
}